from PIL import Image
from random import randint


class TransformImage:
    __grayscale_path = 'img/grayscale.png'

    def __init__(self, image_path: str):
        self.__image: Image = Image.open(image_path)

    def __repr__(self):
        return 'Transform an image to ascii art'

    def __resize(self, new_width: int = None, new_height: int = None):
        """
        resize image if param is not set
        it will keep the base size for the unset value

        :param new_width:
        :param new_height:
        """
        if new_width:
            new_width = new_width if new_width else self.__image.width
            new_width = int(new_height / new_height * new_width)  # keep aspect ratio

        new_height = new_height if new_height else self.__image.height

        self.__image.resize((new_width, new_height))

    def __to_grayscale(self):
        """
        convert image to grayscale
        """
        img_greyscale = self.__image.convert('L')  # grayscale image
        img_greyscale.save(self.__grayscale_path)

    def to_ascii(self):
        """
        create ascii image\n
        :return: None
        """
        # grayscale the image
        self.__to_grayscale()
        image = Image.open(self.__grayscale_path)

        # set the char list for color_slice
        color_slice = self.__create_char_dict()

        # write color_slice in text file
        with open('draw.txt', 'w') as data:
            for y in range(0, image.height):
                for x in range(0, image.width):
                    color = image.getpixel((x, y))
                    for key in color_slice:
                        val_min, val_max = key.split('-')
                        if int(val_min) <= color <= int(val_max):
                            char = color_slice[
                                f'{val_min}-{val_max}'] if x != image.width - 1 else f'{color_slice[f"{val_min}-{val_max}"]}\n'
                            data.write(char)
            data.close()
            image.close()

    def __create_char_dict(self) -> dict:
        """
        create color slice dict to know which character is for which gray shade
        :param rate:
        :return:
        """
        char_list: str = '*-/@$<µ%=)&{!?:._+§'
        char_list_length: int = len(char_list)

        char: dict = {}
        max_color: int = 255
        rate: int = 25

        while max_color > 0:
            max_val: int = max_color - 1 if max_color != 255 else 255
            min_val: int = 0 if max_color - rate < 0 else max_color - rate
            max_color -= rate

            select_char = randint(0, char_list_length - 1)
            char[f'{min_val}-{max_val}'] = char_list[select_char]
            char_list.replace(char_list[select_char], '')

        return char
